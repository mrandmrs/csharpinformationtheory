﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Office.Interop.Excel;
using Microsoft.Vbe.Interop;
using Microsoft.Win32;
using TextBox = System.Windows.Controls.TextBox;
using Window = System.Windows.Window;

namespace Codes
{
    /// <summary>
    /// Класс для кодирования с помощью кода Хэмминга
    /// </summary>
    public class HammingCode
    {
        public class Transmission
        {
            public string Сообщение { get; }
            public string Отправлено { get; }
            public string Принято { get; }
            public string Исправлено { get; }
            public string Статус { get; }


            public Transmission(Code code, List<int> toInvert, HammingCode hamming)
            {
                Сообщение = code.Message;
                Отправлено = code.EncodedMessage;
                StringBuilder sb = new StringBuilder(code.EncodedMessage);
                foreach (int index in toInvert)
                    sb[sb.Length - index] = sb[sb.Length - index] == '0' ? '1' : '0';
                Принято = sb.ToString();

                List<int> codeList = new List<int>();
                foreach (char c in Принято)
                    codeList.Add(int.Parse(c.ToString()));
                int isOdd = 0;
                foreach (int i in codeList.GetRange(0, codeList.Count - 1))
                {
                    isOdd ^= i;
                }
                StringBuilder syndromeStringBuilder = new StringBuilder();
                foreach (ArrayList x in hamming.RunChecks(new ArrayList(codeList),
                    Convert.ToString(codeList.Count - 1, 2).Length))
                    syndromeStringBuilder.Append(x[3]);

                StringBuilder syn = new StringBuilder();
                foreach (char c in syndromeStringBuilder.ToString().Reverse())
                {
                    syn.Append(c);
                }
                string syndrome = syn.ToString();
                int invert = Convert.ToInt32(syndrome, 2) - 1;
                if (Char.Parse(isOdd.ToString()) != Принято[Принято.Length - 1])
                {
                    // Бит
                    if (invert == -1)
                    {
                        sb[sb.Length - 1] = (char) (1 ^ int.Parse(sb[sb.Length - 1].ToString()));
                    }
                    // Синдром
                    else
                    {

                        sb[invert] = char.Parse((1 ^ int.Parse(sb[invert].ToString())).ToString());
                    }
                    Статус = "Одиночная ошибка";
                }
                else
                {
                    // Всё ОК
                    if (invert == -1)
                    {
                        Статус = "Нет ошибок";
                        sb = new StringBuilder("----");
                    }
                    // Двойная
                    else
                    {
                        Статус = "Двойная ошибка!";
                        sb = new StringBuilder("----");
                    }
                }
                Исправлено = sb.ToString();
            }
        }

        public class Code
        {
            public string Message { get; }
            public string EncodedMessage { get; }

            public Code(string message, string code)
            {
                Message = message;
                EncodedMessage = code;
            }

            public override string ToString() => $"{Message} | {EncodedMessage}";
        }

        public class CodingTableRow
        {
            public string Номер_Сообщения { get; }
            public string Оригинальная__последовательность { get; }
            public string Формулы__к_разрядов { get; }
            public string Вычисление__к_разрядов { get; }
            public string К__разряды { get; }
            public string Код { get; }
            public string Бит__четности { get; }
            public string Финальный__код { get; }

            public CodingTableRow(int i, ArrayList original, ArrayList formulaList, ArrayList calcList,
                ArrayList controlsList,
                ArrayList codeList, int evenBit, ArrayList finalCode)
            {
                Номер_Сообщения = i.ToString();

                Оригинальная__последовательность = string.Join(" ", original.ToArray());

                StringBuilder formulaSb = new StringBuilder();
                foreach (ArrayList list in formulaList)
                {
                    formulaSb.Append($"{string.Join(" ⊕ ", list.ToArray())}\n");
                }
                Формулы__к_разрядов = formulaSb.ToString();

                StringBuilder calcSb = new StringBuilder();
                foreach (ArrayList list in calcList)
                {
                    calcSb.Append($"{string.Join(" ⊕ ", list.ToArray())}\n");
                }
                Вычисление__к_разрядов = calcSb.ToString();

                К__разряды = string.Join("\n", controlsList.ToArray());

                Код = string.Join(" ", codeList.ToArray());

                Бит__четности = evenBit.ToString();

                Финальный__код = string.Join(" ", finalCode.ToArray());
            }
        }

        public class HelperChecksRow
        {
            public string Номер__Разряда { get; }
            public string Формула { get; }
            public string Расчет { get; }
            public string Результат { get; }

            public HelperChecksRow(int n, ArrayList elementNames, ArrayList elemntValues, int result)
            {
                Номер__Разряда = n.ToString();
                Формула = string.Join(" ⊕ ", elementNames.ToArray());
                Расчет = string.Join(" ⊕ ", elemntValues.ToArray());
                Результат = result.ToString();
            }
        }

        /// <summary>
        /// Класс строки для вспомогательной таблицы
        /// </summary>
        public class HelperTableRow
        {
            public string Позиция { get; }
            public string Двоичное__Представление { get; }
            public string Соответствие__с_макетом { get; }

            public HelperTableRow(string position, string binaryNumber, string modelLink)
            {
                Позиция = position;
                Двоичное__Представление = binaryNumber;
                Соответствие__с_макетом = modelLink;
            }
        }

        private readonly ArrayList _messages;
        private ArrayList _codes = new ArrayList();
        private int _infoBitsCount;
        private int _controlBitsCount;
        private int _length;
        private ArrayList _modelStrings;
        private ArrayList _helpTableData = new ArrayList();
        private ArrayList _helpChecksData = new ArrayList();
        private ArrayList _codingTable = new ArrayList();

        public ArrayList Codes => _codes;

        public int InfoBitsCount => _infoBitsCount;

        public int ControlBitsCount => _controlBitsCount;

        public int Length => _length;

        public ArrayList ModelStrings => _modelStrings;

        public ArrayList HelpTableData => _helpTableData;

        public ArrayList HelpChecksData => _helpChecksData;

        public ArrayList CodingTable => _codingTable;


        public HammingCode(int messagesCount)
        {
            // Получаем сообщения из их количества
            _messages = MakeMessages(messagesCount);
            CreateCode();
        }

        // Метод-конструктор
        private void CreateCode()
        {
            Debug.Assert(_messages.Count != 0);

            // Считаем Nи, Nк и N
            _infoBitsCount = ((StringBuilder) _messages[0]).Length;
            _controlBitsCount = (int) Math.Ceiling(Math.Log(_infoBitsCount + 1 + Math.Log(_infoBitsCount + 1, 2), 2));
            _length = _controlBitsCount + _infoBitsCount;


            // Создаем Макет кода (наглядный)
            _modelStrings = new ArrayList();

            // Заполняем
            int iCounter = 0;
            int cCounter = 0;
            for (int i = 0; i < _length; i++)
            {
                if (Math.Log(i + 1, 2) % 1 == 0)
                {
                    _modelStrings.Add($"К{++cCounter}");
                }
                else
                {
                    _modelStrings.Add($"И{++iCounter}");
                }
            }

            // Заполняем вспомогательную таблицу
            ArrayList helperTable = MakeMessages(_length, 1);
            for (int i = 0; i < _length; i++)
            {
                _helpTableData.Add(
                    new HelperTableRow($"А{i + 1}", helperTable[i].ToString(), (string) _modelStrings[i]));
            }

            // Проверки
            var checks = RunChecks(helperTable);
            foreach (ArrayList check in checks)
            {
                _helpChecksData.Add(new HelperChecksRow((int) check[0], (ArrayList) check[1], (ArrayList) check[2],
                    (int) check[3]));
            }

            for (int j = 0; j < _messages.Count; j++)
            {
                StringBuilder message = (StringBuilder) _messages[j];
                ArrayList messageList = new ArrayList();
                foreach (char c in message.ToString())
                {
                    messageList.Add(c.ToString());
                }

                ArrayList codeModel = GetCodeModel(message);
                ArrayList code = new ArrayList(codeModel);
                ArrayList codeControls = RunChecks(codeModel, checks.Count);
                ArrayList codeCheckFormulas = new ArrayList();
                ArrayList codeCheckCalcs = new ArrayList();
                ArrayList codeCheckValues = new ArrayList();

                foreach (ArrayList codeControl in codeControls)
                {
                    codeCheckFormulas.Add(codeControl[1]);
                    codeCheckCalcs.Add(codeControl[2]);
                    codeCheckValues.Add(codeControl[3]);
                }

                for (int i = 0; i < codeControls.Count; i++)
                {
                    code[(int) Math.Pow(2, i) - 1] = ((ArrayList) codeControls[i])[3].ToString();
                }
                ArrayList finalCode = new ArrayList(code);
                int sum = 0;
                foreach (string s in finalCode)
                {
                    sum = sum ^ int.Parse(s);
                }
                finalCode.Add(sum.ToString());
                _codingTable.Add(new CodingTableRow(j, messageList, codeCheckFormulas, codeCheckCalcs, codeCheckValues,
                    code, sum, finalCode));
                _codes.Add(new Code(string.Join("", messageList.ToArray()), string.Join("", finalCode.ToArray())));
            }
        }

        private ArrayList RunChecks(ArrayList model, int checksCount)
        {
            int l = checksCount;
            ArrayList resultList = new ArrayList();
            for (int i = 0; i < l; i++)
            {
                ArrayList formulaList = new ArrayList();
                ArrayList calculationList = new ArrayList();
                int result = 0;
//                int result = (int) ((ArrayList) checks[i])[3];
//                formulaList.Add($"Пр{i + 1}");
//                calculationList.Add($"{result}");
                for (int j = (int) Math.Pow(2, i) - 1; j < _length; j += (int) Math.Pow(2, i) * 2)
                {
                    for (int k = 0; k < (int) Math.Pow(2, i) && j + k < _length; k++)
                    {
                        string c = model[j + k].ToString();
                        if (c == "")
                            continue;
                        formulaList.Add($"А{j + k + 1}");
                        calculationList.Add($"{c}");
                        result = result ^ int.Parse(c);
                    }
                }
                resultList.Add(new ArrayList {i + 1, formulaList, calculationList, result});
            }
            return resultList;
        }

        private ArrayList RunChecks(ArrayList helperTable)
        {
            int l = ((StringBuilder) helperTable[0]).Length;
            ArrayList resultList = new ArrayList();
            for (int i = 0; i < l; i++)
            {
                ArrayList formulaList = new ArrayList();
                ArrayList calculationList = new ArrayList();
                int result = 0;
                for (int j = (int) Math.Pow(2, i) - 1; j < _length; j += (int) Math.Pow(2, i) * 2)
                {
                    for (int k = 0; k < (int) Math.Pow(2, i) && j + k < _length; k++)
                    {
                        char c = (helperTable[j + k].ToString())[l - i - 1];
                        formulaList.Add($"А{j + k + 1}");
                        calculationList.Add($"{c}");
                        result = result ^ int.Parse(c.ToString());
                    }
                }
                resultList.Add(new ArrayList {i + 1, formulaList, calculationList, result});
            }
            return resultList;
        }

        private ArrayList GetCodeModel(StringBuilder s)
        {
            ArrayList result = new ArrayList();
            int iCounter = 0;
            for (int i = 0; i < _length; i++)
            {
                if (Math.Log(i + 1, 2) % 1 == 0)
                {
                    result.Add("");
                }
                else
                {
                    result.Add(s[iCounter++].ToString());
                }
            }
            return result;
        }

        public static ArrayList MakeMessages(int messagesCount, int startNumber = 0)
        {
            ArrayList list = new ArrayList();
            StringBuilder last = new StringBuilder(Convert.ToString(startNumber + messagesCount - 1, 2));
            int len = last.Length;
            for (int i = startNumber; i < startNumber + messagesCount - 1; i++)
            {
                StringBuilder s = new StringBuilder(Convert.ToString(i, 2));
                int zeroesToAdd = len - s.Length;
                s.Insert(0, "0", zeroesToAdd);
                list.Add(s);
            }
            list.Add(last);
            return list;
        }
    }

    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private HammingCode _code;

        private HammingCode.Code message;
        private List<int> digits = new List<int>() {1, 2};

        public int MessagesCount
        {
            get => (int) GetValue(MessagesCountProperty);
            set => SetValue(MessagesCountProperty, value);
        }

        public static readonly DependencyProperty MessagesCountProperty =
            DependencyProperty.Register("MessagesCount",
                typeof(int),
                typeof(MainWindow),
                new PropertyMetadata(8));


        public ArrayList HelpTable
        {
            get => (ArrayList) GetValue(HelpTableProperty);
            set => SetValue(HelpTableProperty, value);
        }

        public static readonly DependencyProperty HelpTableProperty =
            DependencyProperty.Register("HelpTable",
                typeof(ArrayList),
                typeof(MainWindow),
                new PropertyMetadata(new ArrayList()));

        public string ControlBits
        {
            get => (string) GetValue(ControlBitsProperty);
            set => SetValue(ControlBitsProperty, value);
        }

        public static readonly DependencyProperty ControlBitsProperty =
            DependencyProperty.Register("ControlBits",
                typeof(string),
                typeof(MainWindow),
                new PropertyMetadata(""));

        public string InfoBits
        {
            get => (string) GetValue(InfoBitsProperty);
            set => SetValue(InfoBitsProperty, value);
        }

        public static readonly DependencyProperty InfoBitsProperty =
            DependencyProperty.Register("InfoBits",
                typeof(string),
                typeof(MainWindow),
                new PropertyMetadata(""));

        public string CodeLength
        {
            get => (string) GetValue(CodeLengthProperty);
            set => SetValue(CodeLengthProperty, value);
        }

        public static readonly DependencyProperty CodeLengthProperty =
            DependencyProperty.Register("CodeLength",
                typeof(string),
                typeof(MainWindow),
                new PropertyMetadata(""));

        public string Model
        {
            get => (string) GetValue(ModelProperty);
            set => SetValue(ModelProperty, value);
        }

        public static readonly DependencyProperty ModelProperty =
            DependencyProperty.Register("Model",
                typeof(string),
                typeof(MainWindow),
                new PropertyMetadata(""));

        public ArrayList Codes
        {
            get => (ArrayList) GetValue(CodesProperty);
            set => SetValue(CodesProperty, value);
        }

        public static readonly DependencyProperty CodesProperty =
            DependencyProperty.Register("Codes",
                typeof(ArrayList),
                typeof(MainWindow),
                new PropertyMetadata(new ArrayList()));

        public ArrayList Checks
        {
            get => (ArrayList) GetValue(ChecksProperty);
            set => SetValue(ChecksProperty, value);
        }

        public static readonly DependencyProperty ChecksProperty =
            DependencyProperty.Register("Checks",
                typeof(ArrayList),
                typeof(MainWindow),
                new PropertyMetadata(new ArrayList()));

        public ArrayList CodingTable
        {
            get => (ArrayList) GetValue(CodingTableProperty);
            set => SetValue(CodingTableProperty, value);
        }

        public static readonly DependencyProperty CodingTableProperty =
            DependencyProperty.Register("CodingTable",
                typeof(ArrayList),
                typeof(MainWindow),
                new PropertyMetadata(new ArrayList()));

        public ObservableCollection<HammingCode.Transmission> TransmissionTable
        {
            get => (ObservableCollection<HammingCode.Transmission>) GetValue(TransmissionTableProperty);
            set => SetValue(TransmissionTableProperty, value);
        }

        public static readonly DependencyProperty TransmissionTableProperty =
            DependencyProperty.Register("TransmissionTable",
                typeof(ObservableCollection<HammingCode.Transmission>),
                typeof(MainWindow),
                new PropertyMetadata(new ObservableCollection<HammingCode.Transmission>()));


        public MainWindow()
        {
            InitializeComponent();
        }

        private void NumberOfMessagesPreview(object sender, TextCompositionEventArgs e)
        {
            Regex numRegex = new Regex("^[0-9]+$");
            e.Handled = !numRegex.IsMatch(e.Text);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _code = new HammingCode(MessagesCount);
            HelpTable = _code.HelpTableData;
            ControlBits =
                $"n_c = log_2(n_i+1+log_2(n_i+1) = log_2({_code.InfoBitsCount} + 1 + log_2{_code.InfoBitsCount} + 1) = {_code.ControlBitsCount}";
            InfoBits = $"n_i = log_2N = {_code.InfoBitsCount}";
            CodeLength = $"n=n_i+n_c = {_code.InfoBitsCount} + {_code.ControlBitsCount} =  {_code.Length}";
            Model = string.Join(" ", _code.ModelStrings.ToArray());
            Codes = _code.Codes;
            Checks = _code.HelpChecksData;
            CodingTable = _code.CodingTable;
        }

        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            if (digits != null && message != null && _code != null)
            {
//                ArrayList transmissionTable = TransmissionTable;
                TransmissionTable.Add(new HammingCode.Transmission(message, digits, _code));
//                transmissionTable.Add(new HammingCode.Transmission(message, digits, _code));
//                TransmissionTable = transmissionTable;
            }
        }

        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count != 0)
                message = (HammingCode.Code) e.AddedItems[0];
        }

        private void ExportExcel(string filename)
        {
            if (_code != null)
            {
                try
                {
                    Microsoft.Office.Interop.Excel.Application xlApp = new
                        Microsoft.Office.Interop.Excel.Application();
                    Workbook workbook = xlApp.Workbooks.Add();
                    for (int i = 0; i < 3; i++)
                        workbook.Worksheets.Add();

                    Microsoft.Office.Interop.Excel.Worksheet worksheet;
                    worksheet = (Microsoft.Office.Interop.Excel.Worksheet) workbook.Worksheets.get_Item(1);
                    int rowIndex;
                    rowIndex = 1;
                    worksheet.Name = "Характеристики";
                    worksheet.Cells[rowIndex++, 1] = "Характеристики";
                    worksheet.Cells[rowIndex, 2] = "n_i";
                    worksheet.Cells[rowIndex++, 3] = _code.InfoBitsCount;
                    worksheet.Cells[rowIndex, 2] = "n_k";
                    worksheet.Cells[rowIndex++, 3] = _code.ControlBitsCount;
                    worksheet.Cells[rowIndex, 2] = "n(Без бита четности)";
                    worksheet.Cells[rowIndex++, 3] = _code.Length;
                    worksheet.Cells[rowIndex, 2] = "n(Учитывая бит четности)";
                    worksheet.Cells[rowIndex++, 3] = _code.Length + 1;
                    worksheet.Cells[rowIndex, 2] = "Макет";
                    worksheet.Cells[rowIndex++, 3] = string.Join(" ", _code.ModelStrings.ToArray());

                    worksheet =
                        (Microsoft.Office.Interop.Excel.Worksheet) workbook.Worksheets.get_Item(2);
                    MakeSheet<HammingCode.HelperTableRow>(worksheet, "Вспомогательная таблица", _code.HelpTableData);
                    worksheet.Columns.AutoFit();
                    worksheet =
                        (Microsoft.Office.Interop.Excel.Worksheet) workbook.Worksheets.get_Item(2);
                    MakeSheet<HammingCode.HelperChecksRow>(worksheet, "Вспомогательная таблица", _code.HelpChecksData,
                        _code.HelpTableData.Count + 4, "Проверки");
                    worksheet =
                        (Microsoft.Office.Interop.Excel.Worksheet) workbook.Worksheets.get_Item(3);
                    int count = 10 > _code.Codes.Count ? _code.Codes.Count : 10;
                    MakeSheet<HammingCode.CodingTableRow>(worksheet, "Кодовая таблица",
                        _code.CodingTable.GetRange(0, count),
                        innerName: $"Пример {count} кодовых комбинаций");


                    workbook.SaveAs(filename);
                    xlApp.Quit();
                }
                catch (System.Runtime.InteropServices.COMException e)
                {
                    MessageBox.Show(this,
                        $"Возникла ошибка: {e}. \nПри невозможности исправить её попробуйте перезагрузить компьютер");
                }
            }
            else
            {
                MessageBox.Show(this, "Сначала нажмите на кнопку \"Кодировать\"");
            }
        }

        private void MakeSheet<userType>(Worksheet worksheet, string worksheetName, ArrayList iterableObject,
            int startIndex = 1, string innerName = null)
        {
            worksheet.Name = worksheetName;
            worksheet.Cells[startIndex++, 1] = innerName != null ? innerName : worksheetName;
            PropertyInfo[] propertyInfos = typeof(userType).GetProperties();
            int columnCounter = 1;
            foreach (PropertyInfo property in propertyInfos)
            {
                worksheet.Cells[startIndex, columnCounter++] = property.Name.Replace("__", " ").Replace("_", " ");
            }
            startIndex++;
            for (int i = 0; i < iterableObject.Count; i++)
            {
                userType o = (userType) iterableObject[i];
                int innerColumnCounter = 1;
                foreach (PropertyInfo property in propertyInfos)
                {
                    worksheet.Cells[i + startIndex, innerColumnCounter++] = $"'{property.GetValue(o)}";
                }
            }
            worksheet.Columns.AutoFit();
//            worksheet.Cells.NumberFormat = "@";
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.OverwritePrompt = false;
            saveFileDialog.DefaultExt = "xlsx";
            saveFileDialog.Filter = "Файлы Excel|*.xlsx";
            bool? fileSelected = saveFileDialog.ShowDialog(this);
            if (fileSelected == true)
            {
                ExportExcel(saveFileDialog.FileName);
            }
        }

        private void TextBoxDigits_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            digits = new List<int>();
            foreach (string s in ((TextBox) sender).Text.Split(new char[] {' '}))
            {
                if (s != "")
                    digits.Add(int.Parse(s));
            }
        }
    }
}